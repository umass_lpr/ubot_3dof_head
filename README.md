# ubot_3_dof_head: #

This repository holds the CAD files (Solidworks) for the head of the uBot-7 mobile manipulator. 
The robot head has 3 degrees of freedom in tilt-pan-tilt configuration and is based on Dynamixel servo motors. The head was originally developed for uBot-7, the latest model of the uBot series, but can be fit to previous uBot versions or other robots. It was developed by the [Laboratory for Perceptual Robotics](http://lpr.cs.umass.edu) at the [University of Massachusetts Amherst](http://www.umass.edu). More information on the head (and the robot) can be found at [http://lpr.cs.umass.edu/ubot](http://lpr.cs.umass.edu/ubot) or in this publication: [uBot-7: A Dynamically Balancing Mobile Manipulator with Series Elastic Actuators](http://lpr.cs.umass.edu/uploads/Robots/ruiken_humanoids_2017.pdf). 

Please note that the design files for the rest of uBot-7 are hosted in a separate repository:
[https://bitbucket.org/umass_lpr/ubot-7](https://bitbucket.org/umass_lpr/ubot-7).

### Contact ###

If you have questions, please contact [Roderic Grupen](http://www-robotics.cs.umass.edu/~grupen) or [Dirk Ruiken](http://www-robotics.cs.umass.edu/~ruiken).


### Citing uBot-7 or the 3-DOF head ###

If you would like to use the uBot-7 models or reference the head or the robot in academic papers, please cite 
[uBot-7: A Dynamically Balancing Mobile Manipulator with Series Elastic Actuators](http://lpr.cs.umass.edu/uploads/Robots/ruiken_humanoids_2017.pdf).